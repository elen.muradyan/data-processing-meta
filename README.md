<!--
*** Thanks for checking out Data Processing Application. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again!
-->

# Data Processing Distributed System

Real-time distributed system to process large amounts of user behavioral data. System will be tracking user clicks on
each screen. After collecting and analyzing all these data system will provide the following statistics:

1. Average clicks count users perform during a given time period.
2. Average time users spent on the appropriate screen.

## Important Note: This project's new milestone is to release version 1, so stay tuned.

<!-- TABLE OF CONTENTS -->

## Table of Contents

<details open="open">
   <ul>
      <li>
         <a href="#technology-stack---other-open-source-libraries">Technology stack &amp; other Open-source libraries</a>
         <ul>
            <li><a href="#data">Data</a></li>
            <li><a href="#jms">JMS</a></li>
            <li><a href="#server---backend">Server - Backend</a></li>
            <li><a href="#libraries-and-plugins">Libraries and Plugins</a></li>
            <li><a href="#containerization">Containerization</a></li>
            <li><a href="#monitoring">Monitoring</a></li>
            <li><a href="#others">Others</a></li>
            <li><a href="#external-tools---services">External Tools &amp; Services</a></li>
         </ul>
      </li>
      <li><a href="#features-and-to-do">Features and To-Do</a></li>
      <li>
         <a href="#getting-started">Getting Started</a>
         <ul>
            <li><a href="#prerequisites">Prerequisites</a></li>
            <li><a href="#architecture">Architecture</a></li>
         </ul>
      </li>
      <li>
         <a href="#installing-and-testing">Installing And Tesing</a>
      </li>
      <li>
         <a href="#deployment">Deployment</a>
      </li>
      <li><a href="#documentation">Documentation</a></li>
      <li>
         <a href="#monitoring">Monitoring</a>
      </li>
      <li>
         <a href="#files-and-directories-structure">Files and Directories Structure</a>
      </li>
      <li><a href="#reporting-issues-suggest-improvements">Reporting Issues/Suggest Improvements</a></li>
      <li><a href="#the-end">The End</a></li>
      <li><a href="#contributing">Contributing</a></li>
      <li><a href="#license">License</a></li>
      <li><a href="#contact">Contact</a></li>
   </ul>
</details>

## Technology stack &amp; other Open-source libraries

### Data

<details open="open">
   <ul>
      <li><a href="https://www.liquibase.org/">Liquibase</a> - Track, version, and deploy database changes</li>
      <li><a href="https://www.postgresql.org/">PostgreSQL</a> - Open-Source Relational Database Management System</li>
      <li><a href="https://www.h2database.com/html/main.html">H2 Database Engine</a> - Java SQL database. Embedded and server modes; in-memory databases</li>
      <li><a href="https://min.io/">Minio</a> - S3 compatible object store</li>
      <li><a href="https://aws.amazon.com/s3/">AWS S3</a> - Amazon Simple Storage Service (Amazon S3) is an object storage service</li>
      <li><a href="https://www.mongodb.com">MongoDB</a> - MongoDB’s document data model naturally supports JSON</li>
      <li><a href="https://github.com/flapdoodle-oss/de.flapdoodle.embed.mongo">Embed.Mongo</a> - Embedded MongoDB will provide a platform neutral way for running mongodb in unittests.</li>
   </ul>
</details>

### JMS

<details open="open">
   <ul>
      <li><a href="https://www.oracle.com/java/technologies/java-message-service.html">JMS</a> - The Java Message Service (JMS) API is a messaging standard that allows application components based on the Java Platform Enterprise Edition (Java EE) to create, send, receive, and read messages.</li>
      <li><a href="https://activemq.apache.org/">ActiveMQ</a> - Apache ActiveMQ® is the most popular open source, multi-protocol, Java-based messaging server./li>
      <li><a href="https://aws.amazon.com/sqs/">Amazon Simple Queue Service</a> - Amazon Simple Queue Service (SQS) is a fully managed message queuing service</li>
   </ul>
</details>

### Server - Backend

<details open="open">
   <ul>
      <li><a href="https://openjdk.java.net/projects/jdk8/">JDK</a> - Java™ Platform, Standard Edition Development Kit</li>
      <li><a href="https://spring.io/projects/spring-boot">Spring Boot</a> - Framework to ease the bootstrapping and development of new Spring Applications</li>
      <li><a href="https://maven.apache.org/">Maven</a> - Dependency Management</li>
   </ul>
</details>

### Libraries and Plugins

<details open="open">
   <ul>
      <li><a href="https://projectlombok.org/">Lombok</a> - Never write another getter or equals method again, with one annotation your class has a fully featured builder, Automate your logging variables, and much more.</li>
      <li><a href="https://swagger.io/">Swagger</a> - Open-Source software framework backed by a large ecosystem of tools that helps developers design, build, document, and consume RESTful Web services.</li>
      <li><a href="https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html">Spring WebFlux</a> - The reactive-stack web framework</li>
      <li><a href="https://github.com/reactor/reactor-core">Reactor Test support</a> - Reactor Test support for unit tests</li>
   </ul>
</details>

### Containerization

<details open="open">
   <ul>
      <li><a href="https://www.docker.com/">Docker</a> - A set of platform as a service products that use OS-level virtualization to deliver software in packages called containers.</li>
      <li><a href="https://kubernetes.io/">Kubernetes</a> - Also known as K8s, is an open-source system for automating deployment, scaling, and management of containerized applications.</li>
      <li><a href="https://aws.amazon.com/eks/">Amazon Elastic Kubernetes Service</a> - Gives you the flexibility to start, run, and scale Kubernetes applications in the AWS cloud or on-premises.</li>
   </ul>
</details>

### Monitoring

<details open="open">
   <ul>
      <li><a href="https://prometheus.io/">Prometheus</a> - Monitoring system and time series database</li>
	  <li><a href="https://grafana.com/">Grafana</a> - Popular technology used to compose observability dashboards with everything from Prometheus</li>
	  <li><a href="https://spring.io/projects/spring-cloud-sleuth">Spring Cloud Sleuth</a> - Logback is intended as a successor to the popular log4j project</li>
	  <li><a href="https://micrometer.io/">Micrometer</a> - Micrometer provides a simple facade over the instrumentation clients for the most popular monitoring systems</li>
	  <li><a href="https://www.jaegertracing.io/">Jaeger</a> - Monitor and troubleshoot transactions in complex distributed systems</li>
   </ul>
</details>

### Others

<details open="open">
   <ul>
      <li><a href="https://git-scm.com/">git</a> - Free and Open-Source distributed version control system</li>
      <li><a href="https://asciidoctor.org/">Asciidoctor</a> - Asciidoctor is a fast, open source text processor and publishing toolchain for converting AsciiDoc content to HTML5.</li>
      <li><a href="https://www.ehcache.org/">Ehcache</a> - Ehcache is an open source, standards-based cache that boosts performance, offloads your database, and simplifies scalability.</li>
   </ul>
</details>

### External Tools & Services

<details open="open">
   <ul>
      <li><a href="https://www.getpostman.com/">Postman</a> - API Development Environment (Testing Documentation)</li>
      <li><a href="https://www.sonarqube.org/sonarlint/">SonarLint</a> - SonarLint is a free IDE extension that finds bugs and vulnerabilities while you code!</li>
      <li><a href="https://checkstyle.sourceforge.io/">Checkstyle</a> - Checkstyle is a development tool to help programmers write Java code that adheres to a coding standard.</li>
   </ul>
</details>

## Features and To-Do

**Important:** please refer to [GitLab Board](https://gitlab.com/data-processing/data-processing-meta/-/boards/2684870)
and [Data Processing Gantt Chart](https://docs.google.com/spreadsheets/d/e/2PACX-1vRnBNODvh_Gz2Nll3ETaGEz0cHx57j092gPWgwVtBTHRMMLlbI1cNZmFNdHfZ3pt9fXZyjW4qzyYiGu/pubhtml)
for up-to-date information and `iterations dates`.

<details open="open">
   <ul>
      <li>[x] [Iteration 1][Data Receiver] Create maven project and add dependencies</li>
      <li>[x] [Iteration 1][Data Receiver] As a user I want to submit event data</li>
      <li>[x] [Iteration 1][Data Receiver] As a system I want to save data to S3 storage</li>
      <li>[x] [Iteration 1][Data Receiver] As a system I want to send data to Data Processor</li>
      <li>[x] [Iteration 1][Data Simulator] Simulates user clicks with timestamp per screen</li>
      <li>[x] [Iteration 1][Data Receiver][Release] Create Dockerfile and update docker-compose.yaml to deploy on local</li>
      <li>[x] [Iteration 2][Data Processor] Create maven project and add dependencies</li>
      <li>[x] [Iteration 2][Data Processor] As a system I want to get data from Data Receiver</li>
      <li>[x] [Iteration 2][Data Processor] As a system I want to filter out event data</li>
      <li>[x] [Iteration 2][Data Processor] As a system I want to save data to relational storage</li>
      <li>[x] [Iteration 2][Data Processor] As a system I want to send data to the Data Analyzer</li>
      <li>[x] [Iteration 2][Data Processor][Release] Create Dockerfile and update docker-compose.yaml to deploy on local</li>
      <li>[x] [Iteration 3][Data Analyzer] Create maven project and add dependencies</li>
      <li>[x] [Iteration 3][Data Analyzer] As a system I want to get data from Data Processor</li>
      <li>[x] [Iteration 3][Data Analyzer] As a system I want to aggregate event data</li>
      <li>[x] [Iteration 3][Data Analyzer] As a system I want to save data to document storage</li>
      <li>[x] [Iteration 3][Data Analyzer] As a user I want to get statistics data by user/screen/date</li>
      <li>[x] [Iteration 3][Data Analyzer][Release] Create Dockerfile and update docker-compose.yaml to deploy on local</li>
      <li>[x] [Iteration 3][Data Simulator] Simulate statistics user requests.</li>
      <li>[x] [Iteration 3][QA] Run docker-compose.yaml and Data Simulator to achieve End to End testing.</li>
      <li>[x] [Iteration 4][Monitoring] Set requestId in the thread context for Receiver/Processor/Analyzer</li>
      <li>[x] [Iteration 4][Monitoring] Configure logback to output logs based on profile also it should read requestId from thread context</li>
      <li>[x] [Iteration 4][Monitoring] Configure spring actuator on Receiver/Processor/Analyzer</li>
      <li>[x] [Iteration 4][Monitoring] Configure Grafana/Prometheus components</li>
      <li>[x] [Iteration 4][Monitoring] Add Grafana/Prometheus components to docker-compose.yaml</li>
      <li>[x] [Iteration 5][AWS] Create AWS account</li>
      <li>[x] [Iteration 5][AWS] Configure S3 and do appropriate changes for prod profile in Receiver</li>
      <li>[x] [Iteration 5][AWS] Configure SQS and do appropriate changes for prod profile in Receiver/Processor/Analyzer</li>
      <li>[x] [Iteration 5][AWS] Configure RDS for Postgres and do appropriate changes for prod profile in Processor</li>
      <li>[x] [Iteration 5][AWS] Configure DocumentDB as document storage provider and do appropriate changes for prod profile in Analyzer</li>
      <li>[x] [Iteration 5][AWS][QA] Run Data Simulator against Receiver/Processor/Analyzer prod profile to achieve End to End testing.</li>
      <li>[x] [Iteration 6][AWS][CI/CD] Configure GitLab to run the pipeline for CI and if CI succeed deploy on AWS EKS</li>
      <li>[x] [Iteration 6][AWS][CI/CD] Configure AWS EKS</li>
      <li>[ ] [Iteration 6][AWS] Add Grafana/Prometheus components to AWS EKS</li>
      <li>[ ] [Iteration 6][AWS] Add CloudFormation</li>
      <li>[ ] [Iteration 7][Future Roadmap] Create a shared library to keep common code</li>
      <li>[ ] [Iteration 7][Future Roadmap] Use AWS CodeArtifact as a maven repository for your project</li>
      <li>[ ] [Iteration 7][Future Roadmap] Configure your projects against the new maven repo and user shared code</li>
      <li>[ ] [Iteration 7][Future Roadmap] [CI/CD] Use git-flow for release and artifacts naming</li>
      <li>[ ] [Iteration 8][Future Roadmap][Data Analyzer][Monetization] Add security by JWT to Analyzer</li>
      <li>[ ] [Iteration 8][Future Roadmap][Data Analyzer][Monetization] Add secure endpoint to retrieve statistics by screen</li>
      <li>[ ] [Iteration 8][Future Roadmap][Data Analyzer][Monetization] Add secure endpoint to retrieve statistics by year/month</li>
      <li>[ ] [Iteration 9][Future Roadmap][Data Processor] Add Cassandra instead of S3 to be able to run queries against Cassandra</li>
      <li>[ ] [Iteration 9][Future Roadmap][Data Processor] Implement ETL from S3 to Cassandra for migration</li>
      <li>[ ] [Iteration 10][Future Roadmap][Data Processor][Data Receiver] Add reindexing of data from Processor to Receiver (entire or by date)</li>
      <li>[ ] [Iteration 10][Future Roadmap][Data Receiver][Data Analyzer] Add reindexing of data from Receiver to Analyzer (entire or by date)</li>
   </ul>
</details>

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Install [Docker](https://docs.docker.com/get-docker/) on your machine
* Install [Meta](https://www.npmjs.com/package/meta) to clone all data-processing repositories

```shell
cd working_dir
# install meta
npm i -g meta
# clone all repositories
meta git clone https://gitlab.com/data-processing/data-processing-meta.git
```

Alternatively:

```shell
cd working_dir
git clone https://gitlab.com/data-processing/data-processing-meta.git
cd data-processing-meta
# clone all repositories
git clone https://gitlab.com/data-processing/data-simulator.git
git clone https://gitlab.com/data-processing/data-receiver.git
git clone https://gitlab.com/data-processing/data-processor.git
git clone https://gitlab.com/data-processing/data-analyzer.git
```

## Architecture

* Refer to [ARCHITECTURE.md](documents/ARCHITECTURE.md) for details.

## Installing And Testing

* Refer to [RUNBOOK.md](documents/RUNBOOK.md) for details.

## Deployment

* For the `CI/CD` and `Cloudformation` instructions please refer to [DEPLOYMENT.md](documents/DEPLOYMENT.md) for
  details.

## Monitoring

Please read before this document about [SLA/SLO/SLI](documents/METRICS.md)

* Refer to [MONITORING.md](documents/MONITORING.md) for details.

## Documentation

* [Data Receiver Swagger](http://localhost:8011/docs/swagger-ui) - `http://localhost:8011/docs/swagger-ui`-
  Documentation & Testing
* [Data Analyzer Swagger](http://localhost:8011/docs/swagger-ui) - `http://localhost:8013/docs/swagger-ui`-
  Documentation & Testing

To look into sample request/response type the following

```shell
$ cd data-processing-meta/data-receiver
$ mvn clean install asciidoctor:process-asciidoc
```

Goto the `target/generated-docs` folder and open `index.html` file. TODO: Copy the `index.html` file to static files
host to make it available through. This should be done during release by GitLab.

## Files and Directories Structure

* Refer to [ARCHITECTURE.md](documents/ARCHITECTURE.md) for details.

## Reporting Issues/Suggest Improvements

This Project uses GitHub's integrated issue tracking system to record bugs and feature requests. If you want to raise an
issue, please follow the recommendations below:

* Before you log a bug,
  please [search the issue tracker](https://gitlab.com/data-processing/data-processing-meta/-/issues?scope=all&utf8=%E2%9C%93&state=all)
  to see if someone has already reported the problem.
* If the issue doesn't already
  exist, [create a new issue](https://gitlab.com/data-processing/data-processing-meta/-/issues/new?issue)
* Please provide as much information as possible with the issue report.
* If you need to paste code, or include a stack trace use Markdown +++```+++ escapes before and after your text.

## The End

In the end, I hope you enjoyed the application and find it useful.

If you would like to enhance, please:

* **Open PRs**,
* Give **feedback**,
* Add **new suggestions**, and
* Finally, give it a 🌟.

* Happy Coding ...* 🙂

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any
contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

Kindly follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) to create an explicit commit
history. Kindly prefix the commit message with one of the following type's.

**build**   : Changes that affect the build system or external dependencies

**ci**      : Changes to our CI configuration files and scripts

**docs**    : Documentation only changes

**feat**    : A new feature

**fix**     : A bug fix

**perf**    : A code change that improves performance

**refactor**: A code change that neither fixes a bug nor adds a feature

**style**   : Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)

**test**    : Adding missing tests or correcting existing tests

## License

Distributed under the MIT License. See [LICENSE.md](/LICENSE.md) for more information.

<!-- CONTACT -->

## Contact

Levon Martirosyan - [@levonmartirosyan](https://twitter.com/levonmartiros) - levon.martirosyan@hotmail.com

Project Link: [Data Processing](https://gitlab.com/data-processing/data-processing-meta)
