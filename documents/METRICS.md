# Service Layer Agreements and User Journeys

## Terminology

* **SLA** - The agreement you made with your client or user.

* **SLO** - The objectives your team must hit to meet that agreement.

* **SLI** - The real numbers of your performance.

## Service Layer Agreements

### 1. Outage

`SaaS server down` - Immediate

### 2. Critical

`High risk of server downtime` - Within 10 minutes

### 3. Urgent

`End-user impact initiated` - Within 20 minutes

### 4. Important

`Potential for performance impact if not addressed` - Within 30 minutes

### 5. Monitor

`Issue addressed but potentially impactful in the future` - Within one business day

### 6. Informational

`Inquiry for information` - Within 48 h

## User Journeys

### User Journey: User Click Load

#### SLI Type: Latency

#### SLI Specification:

Proportion of user click requests that were served in `< 100ms`

(Above, “`user click requests` served in <100ms” is the numerator in the SLI Equation, and “user click requests” is the
denominator.)

#### SLI Implementations:

* Proportion of user click requests served in < 100ms, as measured from the 'latency' column of the server log.

(`Pros/Cons:` This measurement will miss requests that fail to reach the backend.)

* Proportion of user click requests served in `< 100ms`, as measured by probers that execute by Data Simulator running
  in a virtual machine.

(`Pros/Cons:` This will catch errors when requests cannot reach our network, but may miss issues affecting only a subset
of users.)

#### SLO:

`99%` of user click requests in the past 28 days served in < `100ms`.

### User Journey: Get Statistics Load

#### SLI Type: Latency

#### SLI Specification:

Proportion of get statistics requests that were served in `< 200ms`

(Above, “`get statistics requests` served in <200ms” is the numerator in the SLI Equation, and “get statistics requests”
is the denominator.)

#### SLI Implementations:

* Proportion of get statistics requests served in < 200ms, as measured from the 'latency' column of the server log.

(`Pros/Cons:` This measurement will miss requests that fail to reach the backend.)

* Proportion of get statistics requests served in `< 200ms`, as measured by probers that execute by Data Simulator
  running in a virtual machine.

(`Pros/Cons:` This will catch errors when requests cannot reach our network, but may miss issues affecting only a subset
of users.)

#### SLO:

`99%` of get statistics requests in the past 28 days served in < `200ms`.

### User Journey: System CPU Usage

#### SLI Type: System health

#### SLI Specification:

Proportion of application(s) CPU Usage that were served in `< 50%`

(Above, “`application CPU usage` served in <50%” is the numerator in the SLI Equation, and “application CPU usage” is
the denominator.)

#### SLI Implementations:

* Proportion of application CPU usages served in < 50%, as measured from the 'CPU Usage' column of the prometheus.

(`Pros/Cons:` This measurement will miss the data what is the cause of the apllication overload.)

#### SLO:

`99%` of application CPU usages in the past 28 days served in < `50%`.

